"""
Copyright (c) 2023 Wind River Systems, Inc.

SPDX-License-Identifier: Apache-2.0

"""

import logging
import os
import subprocess

from apt_ostree import exceptions

LOG = logging.getLogger(__name__)


def run_command(
    cmd,
    debug=False,
    stdin=None,
    stdout=None,
    stderr=None,
    check=True,
    env=None,
    cwd=None,
):
    """Run a command in a shell."""
    _env = os.environ.copy()
    if env:
        _env.update(env)
    try:
        return subprocess.run(
            cmd,
            stdin=stdin,
            stdout=stdout,
            stderr=stderr,
            env=_env,
            cwd=cwd,
            check=check,
        )
    except FileNotFoundError:
        msg = "%s is not found in $PATH" % cmd[0]
        LOG.error(msg)
        raise exceptions.CommandError(msg)
    except subprocess.CalledProcessError as e:
        msg = "Shell execution error: %s" % e.returncode
        if stderr is not None:
            msg += ", Output: %s" % e.stderr.decode("utf-8")
        LOG.error(msg)
        raise exceptions.CommandError(msg)


def run_sandbox_command(
    args, rootfs, stdin=None, stdout=None, stderr=None, check=True, env=None
):
    """Run a shell wrapped with bwrap."""
    cmd = [
        "bwrap",
        "--proc",
        "/proc",
        "--dev",
        "/dev",
        "--dir",
        "/run",
        "--bind",
        "/tmp",
        "/tmp",
        "--bind",
        f"{rootfs}/boot",
        "/boot",
        "--bind",
        f"{rootfs}/usr",
        "/usr",
        "--bind",
        f"{rootfs}/etc",
        "/etc",
        "--bind",
        f"{rootfs}/var",
        "/var",
        "--symlink",
        "/usr/lib",
        "/lib",
        "--symlink",
        "/usr/lib64",
        "/lib64",
        "--symlink",
        "/usr/bin",
        "/bin",
        "--symlink",
        "/usr/sbin",
        "/sbin",
        "--share-net",
        "--die-with-parent",
        "--chdir",
        "/",
    ]
    cmd += args

    return run_command(
        cmd,
        stdin=stdin,
        stdout=stdout,
        stderr=stderr,
        check=check,
        env=env,
    )
